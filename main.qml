import QtQuick 2.2
import QtQuick.Controls 1.1
import "Methods.js" as Methods

ApplicationWindow {
    id: colorEaterMainWindow
    visible: true
    width: 480
    height: 640
    title: qsTr("Hello World")
    Column {
        id: colorsColumn
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: _buttonsGrid.top
        anchors.bottomMargin: 10
        spacing: 1
        Component.onCompleted: Methods.createColorObjects()
    }
    Grid {
        id: _buttonsGrid
        rows: 1
        columns: 3
        spacing: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom

        ColorButton {
            color: Methods.colors[0]
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Methods.checkObjectWithColor(parent.color)
                }
            }
        }

        ColorButton {
            color: Methods.colors[1]
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Methods.checkObjectWithColor(parent.color)
                }
            }
        }

        ColorButton {
            color: Methods.colors[2]
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Methods.checkObjectWithColor(parent.color)
                }
            }
        }
    }



}
