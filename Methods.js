var component;
var colorBlocks = [];
var colors = [ "#ffafaf" , "#90ee90", "#90ddee" ]

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function createColorObjects() {
    component = Qt.createComponent("ColorBlock.qml");
    if (component.status == Component.Ready)
        finishCreation();
    else
        component.statusChanged.connect(finishCreation);
}

function checkObjectWithColor(color) {
    var i = colorBlocks.length-1
    var item = colorBlocks[i]
    if (color === item.color) {
        item.destroy()
        colorBlocks.splice(i, 1)
//        createColorObjects()
    } else {
        gameOver()
    }
}

function gameOver() {
    console.log("Przegrałes")
}

function finishCreation() {
    if (component.status == Component.Ready) {
        colorBlocks.push(component.createObject(colorsColumn, {color: colors[getRandomInt(0, colors.length-1)]}));
        if (colorBlocks[colorBlocks.length-1] == null) {
            // Error Handling
            console.log("Error creating object");
        }
        if (colorBlocks.length < 20) {
            createColorObjects()
        }
    } else if (component.status == Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }
}
